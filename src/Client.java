/**
 * Created by loubna on 20/11/2016.
 */

import java.util.*;
import java.lang.Enum.*;
enum Gender {
    male,
    female
}
public class Client {

    private int id;
    private String firstName;
    private String lastName;
    private Date birth;
    private Gender sexe;
    private int phone;
    protected Reservation type_reservation;
    private String adr;


    public Client(int id, String firstName, String lastName, Date birth, Gender sexe, int phone, String adr) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birth = birth;
        this.sexe = sexe;
        this.phone = phone;
        this.adr = adr;
    }

    public Reservation getType_reservation() {
        return type_reservation;
    }

    public void setType_reservation(Reservation type_reservation) {
        this.type_reservation = type_reservation;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Gender getSexe() {
        return sexe;
    }

    public void setSexe(Gender sexe) {
        this.sexe = sexe;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }
}
