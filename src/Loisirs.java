/**
 * Created by loubna on 26/12/2016.
 */
public class Loisirs {
    private boolean pool;
    public static float PricePool = (float) 200.00;
    private boolean club;
    public static float PriceClub = (float) 100.00;
    private boolean spa;
    public static float PriceSpa = (float) 150.00;
    private boolean restaurantBreakfast;
    public static float PriceBreakfast = (float) 50.00;
    private boolean restaurantLunch;
    public static float PriceLunch = (float) 50.00;
    private boolean restaurantDinner;
    public static float PriceDinner = (float) 50.00;

    public Loisirs(boolean pool, boolean club, boolean spa, boolean restaurantBreakfast, boolean restaurantLunch, boolean restaurantDinner) {
        this.pool = pool;
        this.club = club;
        this.spa = spa;
        this.restaurantBreakfast = restaurantBreakfast;
        this.restaurantLunch = restaurantLunch;
        this.restaurantDinner = restaurantDinner;
    }

    public boolean isPool() {
        return pool;
    }

    public void setPool(boolean pool) {
        this.pool = pool;
    }

    public boolean isClub() {
        return club;
    }

    public void setClub(boolean club) {
        this.club = club;
    }

    public boolean isSpa() {
        return spa;
    }

    public void setSpa(boolean spa) {
        this.spa = spa;
    }

    public boolean isRestaurantBreakfast() {
        return restaurantBreakfast;
    }

    public void setRestaurantBreakfast(boolean restaurantBreakfast) {
        this.restaurantBreakfast = restaurantBreakfast;
    }

    public boolean isRestaurantLunch() {
        return restaurantLunch;
    }

    public void setRestaurantLunch(boolean restaurantLunch) {
        this.restaurantLunch = restaurantLunch;
    }

    public boolean isRestaurantDinner() {
        return restaurantDinner;
    }

    public void setRestaurantDinner(boolean restaurantDinner) {
        this.restaurantDinner = restaurantDinner;
    }
}
