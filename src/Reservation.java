import java.util.Date;

/**
 * Created by loubna on 26/12/2016.
 */
public class Reservation {
    public Client client;
    private int nbrNights;
    public Room room;
    public Loisirs loisirs;
    private Date Signup;

    public Reservation(Client client, int nbrNights, Room room, Loisirs loisirs, Date signup) {
        this.client = client;
        this.nbrNights = nbrNights;
        this.room = room;
        this.loisirs = loisirs;
        Signup = signup;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getNbrNights() {
        return nbrNights;
    }

    public void setNbrNights(int nbrNights) {
        this.nbrNights = nbrNights;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Loisirs getLoisirs() {
        return loisirs;
    }

    public void setLoisirs(Loisirs loisirs) {
        this.loisirs = loisirs;
    }

    public Date getSignup() {
        return Signup;
    }

    public void setSignup(Date signup) {
        Signup = signup;
    }

    public void extendstay( int nuits) {
        nbrNights += nuits ;
    }
}
