/**
 * Created by loubna on 23/11/2016.
 */
import java.util.ArrayList;
import java.util.Date;
enum  Beds {
    simples,
    doubles
}
public class Room {

    public static float UnitarySingleBedPrice = (float) 3000.00;
    public static float UnitaryDoubleBedPrice = (float) 5000.00;
    private float roomPrice;

    private int nbrLits;
    private ArrayList<Beds> beds;
    private String numb; // numéro étage + chambre
    private int Lvl; // numéro étage
    private int nbRoom; // numéro chambre
    private boolean snack;

    private boolean available;


    public Room(int nbrLits, Beds beds, String numb, int lvl, int nbRoom, boolean snack, boolean available) {
        this.nbrLits = nbrLits;
        //this.beds = beds;
        this.numb = numb;
        Lvl = lvl;
        this.nbRoom = nbRoom;
        this.snack = snack;
        this.available = available;
        calculateRoomPrice();
    }

    /*public Beds getBeds() {
        return beds;
    }

    public void setBeds(Beds beds) {
        this.beds = beds;
    }*/

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setNbrLits(int nbrListe) {
        this.nbrLits = nbrLits;
    }

    public void setNumb(String numb) {
        this.numb = numb;
    }

    public void setLvl(int lvl) {
        Lvl = lvl;
    }

    public void setNbRoom(int nbRoom) {
        this.nbRoom = nbRoom;
    }

    public void setSnack(boolean snack) {
        this.snack = snack;
    }

    public int getNbrLits() {

        return nbrLits;
    }

    public String getNumb() {
        return numb;
    }

    public int getLvl() {
        return Lvl;
    }

    public int getNbRoom() {
        return nbRoom;
    }

    public boolean isSnack() {
        return snack;
    }

    public float getUnitaryPrice() {return roomPrice; }

    public void setUnitaryPrice(float unitaryPrice) {
        this.roomPrice = unitaryPrice; }



    public boolean available() {
        return available;
    }

    public Date availability_date() {
        return null;

    }

    public Date calculateAvailabilityDate(Date Signup, int nbrNights) {

        return null;
    }

    public float calculateRoomPrice(){

        float price = 0;

        for (int i = 0; i < beds.size(); i++) {
            if (beds.get(i) == Beds.simples) price += UnitarySingleBedPrice;
            else
                if (beds.get(i) == Beds.doubles) price += UnitaryDoubleBedPrice;
        }
        return  price; }

}


